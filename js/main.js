$(document).ready(function(){


	$('.arrow').click(function(){
		var stateOpened = $('.window.side').hasClass('show');
		var stateClosed = $('.window.side').hasClass('hide');


		if(stateOpened){
			$('.window.side').removeClass('show');
			$('.window.side').addClass('hide');
			$('.com_block').removeClass('show');
			$('.com_block').addClass('hide');
		}else if(stateClosed){
			$('.window.side').removeClass('hide');
			$('.window.side').addClass('show');
			$('.com_block').removeClass('hide');
			$('.com_block').addClass('show');
		}else{
			$('.window.side').addClass('hide');			
			$('.com_block').addClass('hide');
		}
		
		$('.arrow').toggleClass('open');
		$('.participants').toggleClass('hidden');
		
		
	});

});